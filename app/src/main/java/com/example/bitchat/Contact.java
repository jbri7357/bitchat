package com.example.bitchat;

/**
 * Created by jrbrinkley on 8/4/15.
 */
public class Contact {

    private String mPhoneNumber;
    private String mName;

    public String getPhoneNumber() {
        return mPhoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        mPhoneNumber = phoneNumber;
    }

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        mName = name;
    }
}
