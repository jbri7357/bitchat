# BitChat #
Instant messaging demo app for Android

### Topics addressed: ###

* Using Parse
* Accessing contacts content provider
* Draw9Patch
* Handlers
* Runnables
* Polling a web service
* Writing backwards compatible code